const express = require("express");
const router = express.Router();

const { getAllProducts, testingProducts } = require("../controllers/product");
// Get all Products
router.route("/").get(getAllProducts);

// For Testing Purpose only
router.route("/testing").get(testingProducts);

module.exports = router;
