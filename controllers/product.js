const ProductModel = require("../models/product");

const getAllProducts = async (req, res) => {
  const response = await ProductModel.find({});
  res.status(200).json({ msg: "All Product Details", response });
};

const testingProducts = async (req, res) => {
  res.status(200).json({ msg: "Testing Product Details" });
};
module.exports = { getAllProducts, testingProducts };
